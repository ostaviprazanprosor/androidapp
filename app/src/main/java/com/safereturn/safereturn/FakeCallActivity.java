package com.safereturn.safereturn;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.media.audiofx.BassBoost;
import android.net.Uri;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;

public class FakeCallActivity extends AppCompatActivity {
    private Button mCancelButton = null;
    private Button mAcceptButton = null;
    private Ringtone r = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fake_call);

        mCancelButton = (Button) findViewById(R.id.call_cancel);

        mCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(r!=null) {
                    r.stop();
                }

                finish();
            }
        });

        mAcceptButton = (Button) findViewById(R.id.call_accept);

        mAcceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();

        r = RingtoneManager.getRingtone(getApplicationContext(), Settings.System.DEFAULT_RINGTONE_URI);
        if(r==null) {
            r = RingtoneManager.getRingtone(getApplicationContext(), Settings.System.DEFAULT_ALARM_ALERT_URI);
            if(r==null) {
                r = RingtoneManager.getRingtone(getApplicationContext(), Settings.System.DEFAULT_NOTIFICATION_URI);
                if(r!=null){
                    r.play();
                } else {
                    System.out.println("null");
                }
            } else {
                r.play();
            }
        } else {
            r.play();
        }
    }
}
