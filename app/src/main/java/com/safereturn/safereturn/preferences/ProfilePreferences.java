package com.safereturn.safereturn.preferences;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.safereturn.safereturn.R;

/**
 * Created by maric on 3/29/2016.
 */
public class ProfilePreferences extends Preference {


    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public ProfilePreferences(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    public ProfilePreferences(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public ProfilePreferences(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProfilePreferences(Context context) {
        super(context);
    }

    @Override
    protected View onCreateView(ViewGroup parent) {
        super.onCreateView(parent);

        LayoutInflater li = (LayoutInflater)getContext().getSystemService( Context.LAYOUT_INFLATER_SERVICE );
        return li.inflate(R.layout.pref_profil_image, parent, false);
    }
}