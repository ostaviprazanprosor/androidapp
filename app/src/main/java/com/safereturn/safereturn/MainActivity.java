package com.safereturn.safereturn;

import android.Manifest;
import android.app.AlarmManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.model.Info;
import com.akexorcist.googledirection.model.Leg;
import com.akexorcist.googledirection.model.Route;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safereturn.safereturn.model.Contact;
import com.safereturn.safereturn.model.ContactAdapter;
import com.safereturn.safereturn.model.NotificationAdapter;
import com.safereturn.safereturn.service.PendingApprovalsService;
import com.safereturn.safereturn.service.SafeReturnService;
import com.safereturn.safereturn.service.TimerService;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener, SelfNotificationFragment.OnFragmentInteractionListener, TimeFragment.OnFragmentInteractionListener, NotificationAdapter.NotificationInterface, ContactAdapter.ContactDialogListener, NewContactFragment.OnNewContactFragmentInteraction {
    public static final int LOCATION_REQUEST_CODE = 352353;
    public static final String PENDING_FILENAME = "com.safereturn.safereturn.PENDING";

    private GoogleMap mMap;
    private SafeReturnService mService;
    private boolean mIsBound;
    private ListView mDrawerList = null;
    private DrawerLayout mDrawerLayout = null;
    private ListView mDrawerRightLayout = null;
    private NavigationView mDrawerLeftLayout = null;
    private Toolbar toolbar = null;
    private CircleImageView civ = null;
    private GoogleApiClient gApiClient = null;
    private LatLng mLatLng = null;
    private Polyline mapDirections = null;
    private Marker destMarker = null;
    private Marker startMarker = null;
    private CoordinatorLayout mCoordinatorLayout = null;
    private LocationRequest mLocationRequest = null;
    private CountDownTimer jTimer = null;
    private BottomSheetBehavior behavior = null;
    private BroadcastReceiver receiver = null;
    private ActionBarDrawerToggle actionBarDrawerToggle = null;
    private TextView time_view = null;
    private NotificationAdapter notificationAdapter = null;
    private ContactAdapter contactAdapter;
    private ListView mContactGrid = null;

    private ArrayList<Contact> contactList = null;
    private ArrayList<Contact> notificationList = null;
    private ArrayList<Contact> pendingList = null;

    private int userId;
    private String token;
    private String gcmToken;
    private int nId = 0;

    private long jDuration;
    private boolean journeyStarted = false;

    private int LOCATION_UPDATE_INTERVAL = 10000;
    private int LOCATION_MINIMUM_INTERVAL = 5000;

    private float dpWidth = 0;
    private float dpHeight = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);

        Display display = getWindowManager().getDefaultDisplay();
        DisplayMetrics outMetrics = new DisplayMetrics ();
        display.getMetrics(outMetrics);

        float density  = getResources().getDisplayMetrics().density;
        dpHeight = outMetrics.heightPixels / density;
        dpWidth  = outMetrics.widthPixels / density;

        contactList = new ArrayList<>();
        notificationList = new ArrayList<>();
        pendingList = new ArrayList<>();

        Contact notif = new Contact("marko", "maric", "calemaric@gmail.com", "https://scontent-vie1-1.xx.fbcdn.net/hphotos-ash2/v/t1.0-9/10501587_10203601054139435_2230185595878633753_n.jpg?oh=b6e3b4a50809700fd32084ca3ca05058&oe=575C1615", 1);
        notificationList.add(notif);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                System.out.println(intent.getAction());
                switch (intent.getAction()) {
                    case TimerService.SELF_NOTIFICATION:
                        String type = intent.getStringExtra("type");
                        System.out.println(type);

                        if (type.equals("self")) {
                            System.out.println("markocar");
                            DialogFragment dialog = new SelfNotificationFragment();
                            dialog.show(getSupportFragmentManager(), "SelfNotificationDialog");
                        }
                        abortBroadcast();
                        break;
                    case PendingApprovalsService.ACTION_POLL_RESPONSE:
                        int personAsking = intent.getIntExtra("person_asking", 0);
                        Contact newContact = new Contact(personAsking);
                        newContact.setPending(true);
                        pendingList.add(newContact);
                        notificationList.add(newContact);
                        GetIndividualContact newDetails = new GetIndividualContact(newContact);
                        newDetails.execute(personAsking);
                        abortBroadcast();
                }
            }
        };

        SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);

        userId = session.getInt("id", 0);
        token = session.getString("token", null);

        time_view = (TextView) findViewById(R.id.time_view);

        time_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TimeFragment t = new TimeFragment();
                Bundle arg = new Bundle();
                arg.putString("type", "setNew");
                t.setArguments(arg);
                t.show(getSupportFragmentManager(), "ExtendTime");
            }
        });

        FloatingActionButton fab = (FloatingActionButton)findViewById(R.id.start_button);
        fab.setBackgroundColor(Color.WHITE);

        mContactGrid = (ListView) findViewById(R.id.left_drawer_grid);
        contactAdapter = new ContactAdapter(this, R.id.contact_item, contactList, this);
        mContactGrid.setAdapter(contactAdapter);

        mDrawerLeftLayout = (NavigationView) findViewById(R.id.left_view);
        mDrawerRightLayout = (ListView) findViewById(R.id.right_view);
        mCoordinatorLayout = (CoordinatorLayout) findViewById(R.id.main_coordinator);
        mDrawerLeftLayout.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                if (menuItem.isChecked()) menuItem.setChecked(false);
                else menuItem.setChecked(true);

                mDrawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.inbox:
                        Toast.makeText(getApplicationContext(), "Inbox Selected", Toast.LENGTH_SHORT).show();
                        return true;
                    default:
                        Toast.makeText(getApplicationContext(), "Somethings Wrong", Toast.LENGTH_SHORT).show();
                        return true;
                }
            }
        });

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        actionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                if (drawerView.equals(mDrawerLeftLayout)) {
                    super.onDrawerClosed(drawerView);
                }
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                if (drawerView.equals(mDrawerLeftLayout)) {
                    super.onDrawerSlide(drawerView, slideOffset);
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                if (drawerView.equals(mDrawerLeftLayout)) {
                    super.onDrawerOpened(drawerView);
                }
            }
        };

        mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        notificationAdapter = new NotificationAdapter(this, R.id.notification, notificationList, this);

        mDrawerRightLayout.setAdapter(notificationAdapter);

        Intent intent = new Intent(this, PendingApprovalsService.class);
        intent.setAction(PendingApprovalsService.ACTION_POLL);
        intent.putExtra(PendingApprovalsService.EXTRA_ID, userId);
        startService(intent);

        long scTime = 60 * 10000;

        PendingIntent pendingIntent = PendingIntent.getService(this, 0, intent, 0);

        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarmManager.set(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + scTime, pendingIntent);


        if (gApiClient == null) {
            System.out.println("napravio gapiclient");
            gApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)/*
                    .addApi(Places.GEO_DATA_API)
                    .addApi(Places.PLACE_DETECTION_API)*/
                    .build();
        }

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        if ((civ = (CircleImageView) findViewById(R.id.profile_image)) == null) {
            civ = (CircleImageView) findViewById(R.id.profile_image);
        }

        Picasso.with(this)
                .load("https://scontent-vie1-1.xx.fbcdn.net/hphotos-ash2/v/t1.0-9/10501587_10203601054139435_2230185595878633753_n.jpg?oh=b6e3b4a50809700fd32084ca3ca05058&oe=575C1615")
                .into(civ);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onStart() {
        gApiClient.connect();

        pendingList = loadPendingFromFile();

        if (!notificationList.contains(pendingList)) {
            notificationList.addAll(pendingList);
            notificationAdapter.notifyDataSetChanged();
        }

        System.out.println("sta se desava");

        IntentFilter filter = new IntentFilter();
        filter.addAction(PendingApprovalsService.ACTION_POLL_RESPONSE);
        filter.addAction(TimerService.SELF_NOTIFICATION);
        filter.setPriority(IntentFilter.SYSTEM_HIGH_PRIORITY);

        registerReceiver(receiver, filter);

        if (getIntent().getStringExtra("notification") != null) {
            if (getIntent().getStringExtra("notification").equals("self")) {
                DialogFragment df = new SelfNotificationFragment();
                df.show(getSupportFragmentManager(), "NotificationSelf");
            } else if (getIntent().getStringExtra("notification").equals("other")) {
                Contact notif2 = new Contact("marko", "maric", "calemaric@gmail.com", "https://scontent-vie1-1.xx.fbcdn.net/hphotos-ash2/v/t1.0-9/10501587_10203601054139435_2230185595878633753_n.jpg?oh=b6e3b4a50809700fd32084ca3ca05058&oe=575C1615", 1);
                notificationList.add(notif2);
                System.out.println("sta bre");
                mDrawerLayout.openDrawer(mDrawerRightLayout);
            }
        }

        super.onStart();
    }

    private ArrayList<Contact> loadPendingFromFile() {
        try {
            Gson gson = new Gson();
            FileInputStream fis = openFileInput(MainActivity.PENDING_FILENAME);
            InputStreamReader is = new InputStreamReader(fis);
            BufferedReader input = new BufferedReader(is);
            String response;
            ArrayList<Contact> bla = new ArrayList<>();

            while ((response = input.readLine()) != null) {
                bla = (ArrayList<Contact>) gson.fromJson(response, new TypeToken<ArrayList<Contact>>() {
                }.getType());
            }

            is.close();
            fis.close();
            return bla;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private void loadLatLngFromAddress() {
        Geocoder geoCoder = new Geocoder(this, Locale.getDefault());
        EditText text = (EditText) findViewById(R.id.search_text);
        String address = text.getText().toString();
        try {
            List<Address> addresses = geoCoder.getFromLocationName(address, 5);
            if (addresses.size() > 0) {
                Double lat = addresses.get(0).getLatitude();
                Double lng = addresses.get(0).getLongitude();

                final LatLng latLng = new LatLng(lat, lng);

                getDirectionsFromLatLng(latLng);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        GetContacts contactTask = new GetContacts();
        contactTask.execute();
        super.onResume();
    }

    @Override
    protected void onStop() {
        unregisterReceiver(receiver);
        gApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.right_menu_button) {
            mDrawerLayout.openDrawer(mDrawerRightLayout);
            showNotification("marko car");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
            setMapOptions(mMap);

            View bottomSheet = findViewById(R.id.sheet_below_map);
            behavior = BottomSheetBehavior.from(bottomSheet);
            behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    Log.e("onStateChanged", "onStateChanged:" + newState);
                    if (newState == BottomSheetBehavior.STATE_DRAGGING && !journeyStarted) {
                        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                    Log.e("onSlide", "onSlide");
                }
            });

            if(dpHeight>640) {
                behavior.setPeekHeight(200);
            }
            behavior.setPeekHeight(80);

            setClickListeners();
        } else {
            System.out.println("nema");
        }
    }

    private void setMapOptions(final GoogleMap map) {
        map.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                if (mLatLng != null) {
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
                    mMap.moveCamera(CameraUpdateFactory.zoomTo(15));
                } else {
                    Toast t = Toast.makeText(getApplicationContext(), "Cannot Find Location", Toast.LENGTH_SHORT);
                    t.show();
                }
                return true;
            }
        });

        map.setInfoWindowAdapter(new DestinationInfoAdapter());


        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(final LatLng latLng) {
                getDirectionsFromLatLng(latLng);
            }
        });
    }

    private void getDirectionsFromLatLng(final LatLng latLng) {
        if (mLatLng != null) {
            if (mapDirections != null) {
                mapDirections.remove();
            }

            if (destMarker != null) {
                destMarker.remove();
            }

            GoogleDirection.withServerKey("AIzaSyBlX3Nl7ge-_kvYx5VfjypJa4CHweYnKGQ")
                    .from(mLatLng)
                    .to(latLng)
                    .transportMode(TransportMode.WALKING)
                    .execute(new DirectionCallback() {
                        @Override
                        public void onDirectionSuccess(Direction direction, String rawBody) {
                            if (direction.isOK()) {
                                System.out.println("direction ok");
                                Route route = direction.getRouteList().get(0);
                                Leg leg = route.getLegList().get(0);

                                ArrayList<LatLng> directionPositionList = leg.getDirectionPoint();
                                PolylineOptions polylineOptions = DirectionConverter.createPolyline(getApplicationContext(), directionPositionList, 5, Color.RED);

                                Info distanceInfo = leg.getDistance();
                                Info durationInfo = leg.getDuration();
                                String distance = distanceInfo.getText();
                                String userDuration = durationInfo.getText();
                                jDuration = Long.parseLong(durationInfo.getValue()) * 1000;

                                mapDirections = mMap.addPolyline(polylineOptions);
                                destMarker = mMap.addMarker(new MarkerOptions()
                                        .position(leg.getEndLocation().getCoordination())
                                        .title("Destination")
                                        .snippet(distance + " " + userDuration));

                                //destMarker.showInfoWindow();

                                TextView v = (TextView) findViewById(R.id.sheet_duration);
                                v.setText(distance);

                                TextView start = (TextView) findViewById(R.id.sheet_start);
                                TextView end = (TextView) findViewById(R.id.sheet_destination);

                                Geocoder geocoder;
                                List<Address> startAddresses;
                                List<Address> endAddresses;
                                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());

                                try {
                                    startAddresses = geocoder.getFromLocation(mLatLng.latitude, mLatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                    endAddresses = geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5

                                    String startAddress = startAddresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                    String endAddress = endAddresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                                    start.setText(startAddress);
                                    end.setText(endAddress);

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }

                                Date time = new Date(jDuration);
                                DateFormat format = new SimpleDateFormat("HH:mm:ss");
                                format.setTimeZone(TimeZone.getTimeZone("GMT"));
                                time_view.setText(format.format(time));
                                time_view.animate().translationY(60);
                            } else {
                                System.out.println("direction not ok");
                            }
                        }

                        @Override
                        public void onDirectionFailure(Throwable t) {
                            // Do something
                        }
                    });
        }
    }

    private void setClickListeners() {
        FloatingActionButton danger = (FloatingActionButton) findViewById(R.id.danger_button);
        danger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SendSignal dSignal = new SendSignal();
                dSignal.execute("danger");
            }
        });

        FloatingActionButton start = (FloatingActionButton) findViewById(R.id.start_button);
        start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setStartTimer();
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                journeyStarted = true;
            }
        });

        FloatingActionButton fakeCall = (FloatingActionButton) findViewById(R.id.fake_call);
        fakeCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), FakeCallActivity.class);
                startActivity(i);
            }
        });
    }

    public void setStartTimer() {
        final TextView mDuration = (TextView) findViewById(R.id.sheet_duration);

        if (jTimer != null) {
            jTimer.cancel();
            Intent timerIntent = new Intent(this, TimerService.class);
            timerIntent.setAction(TimerService.ACTION_CANCEL_TIMER);
            startService(timerIntent);

        }

        Intent timerIntent = new Intent(this, TimerService.class);
        timerIntent.setAction(TimerService.ACTION_START_TIMER);
        timerIntent.putExtra(TimerService.EXTRA_TIME, jDuration);
        startService(timerIntent);

        jTimer = new CountDownTimer(jDuration, 1000) {

            public void onTick(long millisUntilFinished) {
                Date time = new Date(millisUntilFinished);
                DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
                formatter.setTimeZone(TimeZone.getTimeZone("GMT"));
                time_view.setText(formatter.format(time));
            }

            public void onFinish() {
                journeyStarted = false;
                time_view.animate().translationY(-60);
            }
        }.start();
    }

    @Override
    public void onConnected(Bundle bundle) {
        System.out.println("google api connected");

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            System.out.println("usao");

            Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                    gApiClient);
            System.out.println(mLastLocation);
            if (mLastLocation != null) {
                System.out.println(mLastLocation.getLatitude());
                mLatLng = new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude());

                mMap.moveCamera(CameraUpdateFactory.newLatLng(mLatLng));
                mMap.moveCamera(CameraUpdateFactory.zoomTo(15));

                MarkerOptions mOptions = new MarkerOptions();
                mOptions.position(mLatLng);

                if (startMarker != null) {
                    startMarker.remove();
                }

                startMarker = mMap.addMarker(mOptions);

            }

            startLocationUpdates();
        } else {
            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(permission, LOCATION_REQUEST_CODE);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("google api connection suspended");
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        System.out.println("google api connection failed");
    }

    protected void startLocationUpdates() {
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(LOCATION_UPDATE_INTERVAL)
                .setFastestInterval(LOCATION_MINIMUM_INTERVAL);

        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(mLocationRequest);

        builder.setAlwaysShow(true);

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(gApiClient, builder.build());
        result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
            @Override
            public void onResult(LocationSettingsResult result) {
                final Status status = result.getStatus();
                final LocationSettingsStates state = result.getLocationSettingsStates();
                switch (status.getStatusCode()) {
                    case LocationSettingsStatusCodes.SUCCESS:
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                                == PackageManager.PERMISSION_GRANTED) {
                            LocationServices.FusedLocationApi.requestLocationUpdates(gApiClient,
                                    mLocationRequest, MainActivity.this);
                        } else {
                            String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                requestPermissions(permission, LOCATION_REQUEST_CODE);
                            }
                        }
                        break;
                    case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                        try {
                            status.startResolutionForResult(
                                    MainActivity.this, 1000);
                        } catch (IntentSender.SendIntentException e) {
                        }
                        break;
                    case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                        System.out.println("SETTINGS_CHANGE_UNAVAILABLE");
                        break;
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case 1000:
                if (resultCode == RESULT_OK) {
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        LocationServices.FusedLocationApi.requestLocationUpdates(gApiClient,
                                mLocationRequest, this);
                    } else {
                        String[] permission = {Manifest.permission.ACCESS_FINE_LOCATION};
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(permission, LOCATION_REQUEST_CODE);
                        }
                    }
                } else {
                    finish();
                }
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        mLatLng = new LatLng(location.getLatitude(), location.getLongitude());

        if (startMarker != null) {
            startMarker.remove();
        }

        MarkerOptions mOptions = new MarkerOptions();
        mOptions.position(mLatLng);
        startMarker = mMap.addMarker(mOptions);
    }

    @Override
    public void onPositiveButtonClick(DialogFragment dialog) {
        SendSignal okSignal = new SendSignal();
        okSignal.execute("cancel");
    }

    @Override
    public void onExtendTime(DialogFragment dialog) {
        TimeFragment pickerFragment = new TimeFragment();
        Bundle args = new Bundle();
        args.putString("type", "extend");
        pickerFragment.setArguments(args);
        pickerFragment.show(getSupportFragmentManager(), "TimePickerFragment");
    }

    @Override
    public void onSetNewTime(int hourOfDay, int minute, DialogFragment dialog, String type) {
        long time = (hourOfDay * 60 + minute) * 60 * 1000;
        System.out.println(time);

        if (type.equals("extend")) {
            Intent timerIntent = new Intent(this, TimerService.class);
            timerIntent.setAction(TimerService.ACTION_START_TIMER);
            timerIntent.putExtra(TimerService.EXTRA_TIME, jDuration);
            startService(timerIntent);
            jDuration = time;

            DateFormat format = new SimpleDateFormat("HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            time_view.setText(format.format(jDuration));
            setStartTimer();

            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            journeyStarted = true;
        } else {
            if (jTimer != null) {
                jTimer.cancel();
                // mService.cancelTimer();
            }

            jDuration = time;
            DateFormat format = new SimpleDateFormat("HH:mm:ss");
            format.setTimeZone(TimeZone.getTimeZone("GMT"));
            time_view.setText(format.format(jDuration));
        }
    }

    @Override
    public void onConfirmButtonClicked(int position) {
        SendSignal s = new SendSignal();
        s.execute("cancel");
        notificationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDismissButtonClicked(int position) {
        notificationList.remove(position);
        notificationAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDeleteButtonClicked(int id) {
        contactList.remove(id);
        contactAdapter.notifyDataSetChanged();
    }

    @Override
    public void onDismissPending(int id) {

    }

    @Override
    public void onConfirmPending(int position) {

    }

    private void savePendingToFile(ArrayList<Contact> pending) {
        try {
            Gson gson = new Gson();
            FileOutputStream fis = openFileOutput(MainActivity.PENDING_FILENAME, MODE_PRIVATE);
            OutputStreamWriter is = new OutputStreamWriter(fis);

            is.write(gson.toJson(pending));
            is.close();
            fis.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addNewContact(View view) {
        mDrawerLayout.closeDrawer(mDrawerLeftLayout);
        behavior.setPeekHeight(0);
        behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        time_view.animate().translationY(-60);
        Fragment contactFragment = new NewContactFragment();
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.map, contactFragment, "NewContactFragment");
        transaction.commit();
    }

    @Override
    public void onNewContactClicked(Contact item) {
        System.out.println(item);
        AddFriendRequest add = new AddFriendRequest();
        add.execute(userId, item.getId());
    }

    @Override
    public void onBackPressed() {
        NewContactFragment fragment = (NewContactFragment) getSupportFragmentManager().findFragmentByTag("NewContactFragment");

        if (fragment != null && fragment.isVisible()) {
            getSupportFragmentManager().beginTransaction()
                    .remove(fragment)
                    .commit();
            behavior.setPeekHeight(80);

            if (jDuration > 0) {
                time_view.animate().translationY(60);
            }
        } else if (mDrawerLayout.isDrawerOpen(mDrawerLeftLayout)) {
            mDrawerLayout.closeDrawer(mDrawerLeftLayout);
        } else if (mDrawerLayout.isDrawerOpen(mDrawerRightLayout)) {
            mDrawerLayout.closeDrawer(mDrawerRightLayout);
        } else {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        }
    }

    public void openSettings(View view) {
        Intent i = new Intent(this, SettingsActivity.class);
        startActivity(i);
    }

    public void openSearch(View view) {
        EditText search =(EditText) findViewById(R.id.search_text);

        if(search.getVisibility()==View.VISIBLE) {
            loadLatLngFromAddress();
        } else if(search.getVisibility()==View.GONE){
           search.setVisibility(View.VISIBLE);
        }
    }

    public void logout(View view) {
        System.out.println(view);
        Intent i = new Intent(this,LoginActivity.class);
        i.putExtra("logout",true);
        startActivity(i);
    }

    private class DestinationInfoAdapter implements GoogleMap.InfoWindowAdapter {

        private final View myContentsView;

        DestinationInfoAdapter() {
            myContentsView = getLayoutInflater().inflate(R.layout.info_window, null);
        }

        @Override
        public View getInfoWindow(Marker marker) {
            TextView text = (TextView) myContentsView.findViewById(R.id.infoText);
            text.setText(marker.getSnippet());
            return myContentsView;
        }

        @Override
        public View getInfoContents(Marker marker) {
            return null;
        }
    }

    private class SendSignal extends AsyncTask<String, Void, Boolean> {
        @Override
        protected Boolean doInBackground(String... params) {
            try {
                SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
                gcmToken = session.getString("gcm-token", "");

                URL url = new URL(TimerService.SERVER_ENDPOINT + "danger/" + userId + "/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("PUT");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "application/json");

                JSONObject requestParams = new JSONObject();

                switch (params[0]) {
                    case "danger":
                        requestParams.put("in_danger", true);
                        break;
                    case "cancel":
                        requestParams.put("in_danger", false);
                        break;
                }
                requestParams.put("gcm_token", gcmToken);
                requestParams.put("token", token);

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();

                int httpResponse = connection.getResponseCode();

                System.out.println(httpResponse);

                connection.disconnect();
                return true;
            } catch (JSONException | IOException e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            if (success) {
                Toast toast = Toast.makeText(getApplicationContext(), "Danger Signal Sent", Toast.LENGTH_SHORT);
                toast.show();
            } else {
                Toast toast = Toast.makeText(getApplicationContext(), "Danger Signal Failed", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    private class AddFriendRequest extends AsyncTask<Integer, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Integer... params) {
            try {
                URL url = new URL(TimerService.SERVER_ENDPOINT + "contact/");

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("POST");
                connection.setDoOutput(true);
                connection.setDoInput(true);
                connection.setRequestProperty("Content-Type", "application/json");

                JSONObject requestParams = new JSONObject();

                requestParams.put("for_who", params[0]);
                requestParams.put("related_contact", params[1]);
                requestParams.put("is_approved", false);
                //requestParams.put("token", token);

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();

                int httpResponse = connection.getResponseCode();

                System.out.println("addFriend " + httpResponse);

                if (httpResponse == HttpURLConnection.HTTP_OK) {
                    Toast.makeText(MainActivity.this, "Friend Request Added", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Friend Request Failed", Toast.LENGTH_SHORT).show();
                }

                connection.disconnect();
                return true;
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
            return false;
        }
    }

    private class GetContacts extends AsyncTask<Void, Void, String> {
        @Override
        protected String doInBackground(Void... params) {
            try {
                SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
                gcmToken = session.getString("gcm-token", "");
                int id = session.getInt("id", 0);

                URL url = new URL(TimerService.SERVER_ENDPOINT + "contact/" + id);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(false);
                connection.setDoInput(true);

                int httpResponse = connection.getResponseCode();

                System.out.println("get contacts: " + httpResponse);

                if (httpResponse == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String input;
                    while ((input = reader.readLine()) != null) {
                        sb.append(input);
                    }
                    return sb.toString();
                }

                connection.disconnect();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String data) {
            if (data != null) {
                try {
                    System.out.println(data);
                    JSONArray dataArray = new JSONArray(data);

                    for (int i = 0; i < dataArray.length(); i++) {
                        JSONObject preCast = dataArray.getJSONObject(i);
                        int id = preCast.getInt("related_contact");

                        Contact contact = new Contact(id);

                        if (!contactList.contains(contact)) {
                            contactList.add(contact);
                        }

                        GetIndividualContact personDetail = new GetIndividualContact(contact);
                        personDetail.execute(contact.getId());
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class GetIndividualContact extends AsyncTask<Integer, Void, String> {
        private Contact contact = null;

        public GetIndividualContact(Contact contact) {
            this.contact = contact;
        }

        @Override
        protected String doInBackground(Integer... params) {
            try {
                SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
                gcmToken = session.getString("gcm-token", "");
                int id = session.getInt("id", 0);

                URL url = new URL(TimerService.SERVER_ENDPOINT + "personDetail/" + params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(false);
                connection.setDoInput(true);

                int httpResponse = connection.getResponseCode();

                System.out.println("person detail " + httpResponse);

                if (httpResponse == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String input;
                    while ((input = reader.readLine()) != null) {
                        sb.append(input);
                    }
                    return sb.toString();
                }
                connection.disconnect();
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if (s != null) {
                try {
                    JSONObject mainObject = new JSONObject(s);
                    String inDanger = mainObject.getString("in_danger");

                    JSONObject user = mainObject.getJSONObject("user");

                    String firstName = user.getString("first_name");
                    String lastName = user.getString("last_name");
                    String email = user.getString("email");

                    contact.setEmail(email);
                    contact.setFirst_name(firstName);
                    contact.setLast_name(lastName);
                    contactAdapter.notifyDataSetChanged();
                    notificationAdapter.notifyDataSetChanged();
                    System.out.println("pending: " + pendingList);

                    savePendingToFile(pendingList);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }


    private void showNotification(String data) {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Friend In Danger!")
                        .setContentText(data + " is in danger!")
                        .setSmallIcon(R.drawable.ic_notifications_none);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("name", data);
        resultIntent.putExtra("notification", "other");

        System.out.println("usoa u notifikacije");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(nId, mBuilder.build());
        nId++;
    }
}
