package com.safereturn.safereturn;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.safereturn.safereturn.service.IdListenerService;
import com.safereturn.safereturn.service.TimerService;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * A login screen that offers login via username/password.
 */
public class LoginActivity extends AppCompatActivity {
    private UserLoginTask mAuthTask = null;

    private AutoCompleteTextView mUsernameView = null;
    private EditText mPasswordView = null;
    private View mProgressView = null;
    private BottomSheetBehavior behavior = null;
    private boolean logout = false;

    private SharedPreferences loginInfo = null;
    private SharedPreferences session = null;

    private Button mUsernameSignInButton = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mUsernameView = (AutoCompleteTextView) findViewById(R.id.username);

        TextView zabSifru = (TextView) findViewById(R.id.forgotPass);
        zabSifru.setTextColor(Color.parseColor("#FFFFFF"));

        Button btmPrijaviSe= (Button)findViewById(R.id.loginButton);
        btmPrijaviSe.setTextColor(Color.parseColor("#FFFFFF"));

        TextView nemamNalog = (TextView) findViewById(R.id.textNemateNalog);
        nemamNalog.setTextColor(Color.parseColor("#FFFFFF"));

        TextView btmRegSe = (TextView) findViewById(R.id.textRegistrujSe);
        btmRegSe.setTextColor(Color.parseColor("#6ED7FA"));

        mUsernameView.setHintTextColor(Color.WHITE);

        logout = getIntent().getBooleanExtra("logout",false);

        System.out.println(logout);

        TextView mTextView = (TextView) findViewById(R.id.textIspodLogoa);
        mTextView.setTextColor(Color.parseColor("#FFFFFF"));

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    return true;
                }
                return false;
            }
        });

        mUsernameSignInButton = (Button) findViewById(R.id.loginBtn);
        mUsernameSignInButton.setTextColor(Color.parseColor("#FFFFFF"));



        Button mRegisterButton = (Button) findViewById(R.id.registerButton);
        mRegisterButton.setTextColor(Color.parseColor("#FFFFFF"));

        mRegisterButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getApplicationContext(),RegisterActivity.class);

                startActivity(i);
            }
        });

        mProgressView = findViewById(R.id.login_progress);

        session = getSharedPreferences("session", MODE_PRIVATE);

        loginInfo = getSharedPreferences(getString(R.string.login_info), Context.MODE_PRIVATE);


        if(logout) {
            System.out.println("blabla");
            SharedPreferences.Editor editor = loginInfo.edit();
            editor.clear();
            editor.apply();

            SharedPreferences.Editor editor1 = session.edit();
            editor1.clear();
            editor1.apply();
        }

        if(!loginInfo.getString("username","").equals("") && !loginInfo.getString("password","").equals("")) {
            showProgress(true);
            new UserLoginTask(loginInfo.getString("username", ""),loginInfo.getString("password","")).execute((Void) null);
        }



    }

    @Override
    protected void onStart() {
        super.onStart();

        CoordinatorLayout coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        View bottomSheet = coordinatorLayout.findViewById(R.id.bottom_sheet_login);

        System.out.println(bottomSheet);

        behavior = BottomSheetBehavior.from(bottomSheet);

        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                Log.e("onStateChanged", "onStateChanged:" + newState);


            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                Log.e("onSlide", "onSlide");
            }
        });

        mUsernameSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("usao u listener");
                System.out.println(behavior.getState());
                behavior.setState(BottomSheetBehavior.STATE_EXPANDED);

            }
        });

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid username, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    public void attemptLogin(View view) {

        if (mAuthTask != null) {
            return;
        }

        mUsernameView.setError(null);
        mPasswordView.setError(null);

        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }

        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        } else if (!isUsernameValid(username)) {
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUsernameValid(String username) {
        //TODO: Replace this with your own logic
        return true;
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        return password.length() > 4;
    }

    public void register(View view) {
        Intent i = new Intent(getApplicationContext(),RegisterActivity.class);
        startActivity(i);
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if(behavior.getState()==BottomSheetBehavior.STATE_EXPANDED) {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        } else {
            finish();
            super.onBackPressed();
        }
    }

    public void openBottomSheet(View view) {
        System.out.println("usao u listener");
        System.out.println(behavior.getState());
        if (behavior.getState() == BottomSheetBehavior.STATE_HIDDEN) {
            behavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        } else {
            behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        }
    }

    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String mUsername;
        private final String mPassword;

        UserLoginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected void onPreExecute() {
            showProgress(true);
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {
                URL loginUrl = new URL(TimerService.SERVER_ENDPOINT + "token-auth/");

                HttpURLConnection connection = (HttpURLConnection) loginUrl.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setDoInput(true);
                connection.setDoOutput(true);

                JSONObject requestParams = new JSONObject();
                requestParams.put("username", mUsername);
                requestParams.put("password", mPassword);

                OutputStreamWriter writer = new OutputStreamWriter(connection.getOutputStream());
                writer.write(requestParams.toString());
                writer.flush();

                int httpResponse = connection.getResponseCode();
                System.out.println(httpResponse);
                System.out.println(connection.getResponseMessage());

                StringBuilder sb = new StringBuilder();
                if(httpResponse == HttpURLConnection.HTTP_OK) {
                    System.out.println("bla");
                    BufferedReader input = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    System.out.println(input);
                    String line;

                    while ((line = input.readLine()) != null) {
                        System.out.println(line);
                        sb.append(line + "\n");
                    }

                    input.close();

                    JSONObject response = new JSONObject(sb.toString());
                    int id = response.getInt("id");
                    String token = response.getString("token");

                    System.out.println("login id "+id);

                    SharedPreferences.Editor edit = session.edit();
                    edit.putInt("id", id);
                    edit.putString("token", token);
                    edit.commit();

                    System.out.println("dosao dovde");
                    return true;
                } else {
                    System.out.println(connection.getResponseMessage());
                    return false;
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e1) {
                e1.printStackTrace();
                return false;
            } catch (JSONException e2) {
                e2.printStackTrace();
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            System.out.println("marko "+success);

            if (success) {
                SharedPreferences.Editor edit = loginInfo.edit();
                edit.putString("username", mUsername);
                edit.putString("password",mPassword);
                edit.commit();

                System.out.println(mUsername);
                System.out.println(mPassword);

                Intent idListener = new Intent(getApplicationContext(),IdListenerService.class);
                startService(idListener);
                System.out.println("Id service started");

                Intent i = new Intent(getApplicationContext(),MainActivity.class);
                startActivity(i);
            } else {
                mPasswordView.setError(getString(R.string.error_incorrect_password));
                mPasswordView.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }
}

