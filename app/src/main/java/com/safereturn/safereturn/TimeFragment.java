package com.safereturn.safereturn;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.TimePicker;


/**
 * Created by maric on 3/18/2016.
 */
public class TimeFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    private OnFragmentInteractionListener mListener;
    private String type;

    public TimeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        type = getArguments().getString("type");
        return new TimePickerDialog(getActivity(),this,0,0,true);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        System.out.println("timePicked");
        System.out.println(type);
        mListener.onSetNewTime(hourOfDay,minute,TimeFragment.this,type);
    }

    public interface OnFragmentInteractionListener {
        void onSetNewTime(int hourOfDay,int minute,DialogFragment dialog,String type);
    }
}


