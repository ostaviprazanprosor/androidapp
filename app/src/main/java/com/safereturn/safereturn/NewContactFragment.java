package com.safereturn.safereturn;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.safereturn.safereturn.model.Contact;
import com.safereturn.safereturn.model.MyNewContactAdapter;
import com.safereturn.safereturn.service.TimerService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link com.safereturn.safereturn.NewContactFragment.OnNewContactFragmentInteraction}
 * interface.
 */
public class NewContactFragment extends Fragment {
    private static final String ARG_COLUMN_COUNT = "column-count";
    private static final String ARG_CONTACTS = "contacts";

    private int mColumnCount = 1;
    private ArrayList<Contact> mContacts = null;
    private EditText mSearch = null;
    private Button mSearchButton = null;
    private OnNewContactFragmentInteraction mListener;
    private MyNewContactAdapter mContactsAdapter = null;

    public NewContactFragment() {
    }

    public static NewContactFragment newInstance(int columnCount) {
        NewContactFragment fragment = new NewContactFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContacts = new ArrayList<>();

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_contact_list, container, false);

        mSearch = (EditText) view.findViewById(R.id.search_text);
        mSearchButton = (Button) view.findViewById(R.id.new_search_button);
        mContactsAdapter = new MyNewContactAdapter(mContacts, mListener);

        System.out.println("usao u view instanceof");
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.list);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        recyclerView.setAdapter(mContactsAdapter);


        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("pre if");
                if(mSearch.getText()!=null && !mSearch.getText().toString().equals("")) {
                    System.out.println("usao u searchf");
                    mContacts.removeAll(mContacts);
                    mContactsAdapter.notifyDataSetChanged();
                    SearchContact search = new SearchContact();
                    search.execute(mSearch.getText().toString());
                    System.out.println("execute");
                }
            }
        });
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnNewContactFragmentInteraction) {
            mListener = (OnNewContactFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnNewContactFragmentInteraction {
        void onNewContactClicked(Contact item);
    }


    private class SearchContact extends AsyncTask<String,Void,String> {
        @Override
        protected String doInBackground(String... params) {
            try {
                System.out.println("blabl ablksdj");
                SharedPreferences session = getActivity().getSharedPreferences("session", MainActivity.MODE_PRIVATE);

                URL url = new URL(TimerService.SERVER_ENDPOINT + "search/?name="+params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(false);
                connection.setDoInput(true);
                System.out.println("marko");

                int httpResponse = connection.getResponseCode();
                System.out.println("marko");

                if(httpResponse==HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

                    StringBuilder sb = new StringBuilder();
                    String response;

                    while((response = reader.readLine())!=null) {
                        sb.append(response);
                    }
                    return sb.toString();
                } else {
                    System.out.println("nije uspeo search iz nekog razloga");
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String response) {
            if(response!=null) {
                try {
                    JSONArray array = new JSONArray(response);

                    for(int i=0;i<array.length();i++) {
                        JSONObject enc = array.getJSONObject(i);

                        JSONObject user = enc.getJSONObject("user");

                        int id = user.getInt("id");
                        Contact search = new Contact(id);
                        if(!mContacts.contains(search)) {
                            mContacts.add(search);

                            GetSearchContact getIndividualContact = new GetSearchContact(search);
                            getIndividualContact.execute(id);
                        }
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }
    }

    private class GetSearchContact extends AsyncTask<Integer,Void,String> {
        private Contact contact = null;

        public GetSearchContact(Contact contact) {
            this.contact = contact;
        }

        @Override
        protected String doInBackground(Integer... params) {
            try {
                URL url = new URL(TimerService.SERVER_ENDPOINT + "personDetail/"+params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(false);
                connection.setDoInput(true);

                int httpResponse = connection.getResponseCode();

                System.out.println("person detail " + httpResponse);

                if(httpResponse == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String input;
                    while((input = reader.readLine())!=null) {
                        sb.append(input);
                    }
                    return sb.toString();
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null) {
                try {
                    JSONObject mainObject = new JSONObject(s);
                    String inDanger = mainObject.getString("in_danger");

                    JSONObject user = mainObject.getJSONObject("user");

                    String firstName = user.getString("first_name");
                    String lastName = user.getString("last_name");
                    String email = user.getString("email");

                    contact.setEmail(email);
                    contact.setFirst_name(firstName);
                    contact.setLast_name(lastName);

                    System.out.println(mContacts);
                    mContactsAdapter.notifyDataSetChanged();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }
}
