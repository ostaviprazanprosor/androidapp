package com.safereturn.safereturn.service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.safereturn.safereturn.MainActivity;
import com.safereturn.safereturn.R;

public class SafeReturnService extends Service {
    public static final String serverEndpoint = "http://192.168.0.10:8000/api/";
    private int id;
    private int nId = 0;

    public static final String SELF_NOTIFICATION = "com.safereturn.SelfNotificationService.SELF_NOTIFICATION";

    private Handler handler = null;
    private LocalBroadcastManager localBroadcastManager = null;

    private Runnable executeNotification = new Runnable() {
        @Override
        public void run() {
            System.out.println("usao u execute notification");
            Intent i = new Intent(SELF_NOTIFICATION);
            i.putExtra("type","self");
            localBroadcastManager.sendBroadcast(i);
            showNotification();
        }
    };

    public class LocalBinder extends Binder {
        public SafeReturnService getService() {
            return SafeReturnService.this;
        }
    }

    @Override
    public void onCreate() {
        localBroadcastManager = LocalBroadcastManager.getInstance(this);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i("SafeReturnService", "Received start id " + startId + ": " + intent);
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mBinder;
    }

    private final IBinder mBinder = new LocalBinder();

    public void setTimer(long time) {
        System.out.println("setovao timer");
        handler = new Handler();
        handler.postDelayed(executeNotification,time);
        System.out.println(time);
    }

    private void showNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Timer is up!")
                        .setContentText("Have you arrived to your destination?")
                        .setSmallIcon(R.drawable.ic_notifications_none);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("notification","self");

        System.out.println("usoa u notifikacije");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(nId, mBuilder.build());
        nId++;
    }

    public void extendTimer(long time) {
        handler.postDelayed(executeNotification,time);
    }

    public void cancelTimer() {
        handler.removeCallbacks(executeNotification);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
