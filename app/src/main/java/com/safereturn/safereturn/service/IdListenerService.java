package com.safereturn.safereturn.service;

import android.app.IntentService;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.iid.InstanceIDListenerService;
import com.safereturn.safereturn.R;

import java.io.IOException;
import java.net.IDN;

public class IdListenerService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public IdListenerService(String name) {
        super(name);
    }

    public IdListenerService() {
        super("IdListenerService");
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        InstanceID instanceID = InstanceID.getInstance(this);
        System.out.println(instanceID);
        try {
            String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                    GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);

            SharedPreferences session = getSharedPreferences("session", Context.MODE_PRIVATE);

            SharedPreferences.Editor edit = session.edit();
            edit.putString("gcm-token", token);
            edit.commit();

            System.out.println("token "+token);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
