package com.safereturn.safereturn.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.safereturn.safereturn.MainActivity;
import com.safereturn.safereturn.model.Contact;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class PendingApprovalsService extends IntentService {
    public static final String ACTION_POLL = "com.safereturn.safereturn.service.action.POLL";
    public static final String ACTION_POLL_RESPONSE = "com.safereturn.safereturn.service.action.RESPONSE";
    public static final String ACTION_DELETE = "com.safereturn.safereturn.service.action.DELETE";

    public static final String EXTRA_ID = "com.safereturn.safereturn.service.extra.PARAM1";

    private ArrayList<Contact> pendingList = null;


    public PendingApprovalsService() {
        super("PendingApprovalsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        System.out.println("pending");
        pendingList = getPendingFromFile();
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_POLL.equals(action)) {
                int param1 = intent.getIntExtra(EXTRA_ID,0);
                System.out.println("param" + param1);
                handleActionPoll(param1);
            }
        }
    }

    private ArrayList<Contact> getPendingFromFile() {
        try {
            Gson gson = new Gson();
            FileInputStream fis = openFileInput(MainActivity.PENDING_FILENAME);
            InputStreamReader is = new InputStreamReader(fis);
            BufferedReader input = new BufferedReader(is);
            String response;
            ArrayList<Contact> bla = new ArrayList<>();

            while((response = input.readLine())!=null) {
                bla = (ArrayList<Contact>) gson.fromJson(response, new TypeToken<ArrayList<Contact>>(){}.getType());
            }

            is.close();
            fis.close();
            return bla;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private void handleActionPoll(int id) {
        HttpURLConnection connection = null;

        try {
            SharedPreferences session = getSharedPreferences("session", MODE_PRIVATE);
            String gcmToken = session.getString("gcm-token", "");

            URL url = new URL(TimerService.SERVER_ENDPOINT + "pendingApprovals/" + id+"/");

            connection = (HttpURLConnection) url.openConnection();

            connection.setRequestMethod("GET");
            connection.setDoOutput(false);
            connection.setDoInput(true);

            int httpResponse = connection.getResponseCode();

            StringBuilder sb = new StringBuilder();

            if (httpResponse == HttpURLConnection.HTTP_OK) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                String input;
                while ((input = reader.readLine()) != null) {
                    sb.append(input);
                }

                JSONArray mainObject = new JSONArray(sb.toString());

                for(int i =0;i<mainObject.length();i++) {
                    JSONObject subObject = mainObject.getJSONObject(i);
                    int personAsking = subObject.getInt("person_asking");
                    int personAsked = subObject.getInt("person_asked");

                    Contact pending = new Contact(personAsking);

                    if(!pendingList.contains(pending)) {
                        System.out.println("usao u pending");
                        GetSearchContact search = new GetSearchContact(pending);
                        search.execute(personAsking);
                    }
                }
            } else {
                System.out.println("nije uspeo pending");
            }

            connection.disconnect();
        } catch (IOException | JSONException e) {
            if(connection!=null) {
                connection.disconnect();
            }
            e.printStackTrace();
        }
    }

    private class GetSearchContact extends AsyncTask<Integer,Void,String> {
        private Contact contact = null;

        public GetSearchContact(Contact contact) {
            this.contact = contact;
        }

        @Override
        protected String doInBackground(Integer... params) {
            try {
                URL url = new URL(TimerService.SERVER_ENDPOINT + "personDetail/"+params[0]);

                HttpURLConnection connection = (HttpURLConnection) url.openConnection();

                connection.setRequestMethod("GET");
                connection.setDoOutput(false);
                connection.setDoInput(true);

                int httpResponse = connection.getResponseCode();

                System.out.println("person detail " + httpResponse);

                if(httpResponse == HttpURLConnection.HTTP_OK) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String input;
                    while((input = reader.readLine())!=null) {
                        sb.append(input);
                    }

                    return sb.toString();
                }
                return null;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            if(s!=null) {
                try {
                    JSONObject mainObject = new JSONObject(s);
                    String inDanger = mainObject.getString("in_danger");

                    JSONObject user = mainObject.getJSONObject("user");

                    String firstName = user.getString("first_name");
                    String lastName = user.getString("last_name");
                    String email = user.getString("email");

                    contact.setEmail(email);
                    contact.setFirst_name(firstName);
                    contact.setLast_name(lastName);

                    Intent intent = new Intent();
                    intent.setAction(ACTION_POLL_RESPONSE);
                    intent.putExtra("person_asking", contact.getId());
                    intent.putExtra("first_name", contact.getFirst_name());
                    intent.putExtra("last_name",contact.getLast_name());
                    sendBroadcast(intent);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            super.onPostExecute(s);
        }
    }
}
