package com.safereturn.safereturn.service;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.safereturn.safereturn.MainActivity;
import com.safereturn.safereturn.R;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions and extra parameters.
 */
public class TimerService extends Service {
/*
    public static final String SERVER_ENDPOINT = "http://safe-return-dkeske.c9users.io/api/";
*/
    public static final String SERVER_ENDPOINT = "http://192.168.1.4:8000/api/";
    public static final String ACTION_START_TIMER = "com.safereturn.safereturn.service.action.START_TIMER";
    public static final String ACTION_CANCEL_TIMER = "com.safereturn.safereturn.service.action.CANCEL_TIMER";

    public static final String EXTRA_TIME = "com.safereturn.safereturn.service.extra.TIME";
    public static final String SELF_NOTIFICATION = "com.safereturn.SelfNotificationService.SELF_NOTIFICATION";

    private int nId = 0;
    private Handler handler = null;

    private Runnable executeNotification = new Runnable() {
        @Override
        public void run() {
            System.out.println("usao u execute notification");
            Intent i = new Intent(SELF_NOTIFICATION);
            i.putExtra("type", "self");
            sendBroadcast(i);
            showNotification();
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        String action = intent.getAction();
        if(action.equals(ACTION_START_TIMER)) {
            handleActionStartTimer(intent.getLongExtra(EXTRA_TIME,0));
        } else if(action.equals(ACTION_CANCEL_TIMER)) {
            handleActionCancelTimer();
        }
        return super.onStartCommand(intent, flags, startId);
    }

    private void handleActionStartTimer(Long time) {
        handler = new Handler();
        handler.postDelayed(executeNotification,time);

        System.out.println("sto[sdf");
    }

    private void handleActionCancelTimer() {
        if(handler!=null) {
            handler.removeCallbacks(executeNotification);
        }
        stopSelf();

        System.out.println("zaustavio");
    }

    private void showNotification() {
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setContentTitle("Timer is up!")
                        .setContentText("Have you arrived to your destination?")
                        .setSmallIcon(R.drawable.ic_notifications_none);

        Intent resultIntent = new Intent(this, MainActivity.class);
        resultIntent.putExtra("notification","self");

        System.out.println("usoa u notifikacije");

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(nId, mBuilder.build());
        nId++;
    }

}
