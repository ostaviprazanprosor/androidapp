package com.safereturn.safereturn.model;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.safereturn.safereturn.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by maric on 3/23/2016.
 */
public class NotificationAdapter extends ArrayAdapter<Contact> {
    private ViewHolder viewHolder = null;
    private NotificationInterface nInterface = null;

    private static class ViewHolder {
        private TextView mFirstName;
        private TextView mLastName;
        private CircleImageView mImage;
        private TextView mEmail;
    }

    public NotificationAdapter(Context context, int resource, ArrayList<Contact> objects, NotificationInterface nInterface) {
        super(context, resource, objects);
        this.nInterface = nInterface;
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public View getView(final int position, View convertView, ViewGroup parent) {
        final Contact item = getItem(position);

        if (convertView == null) {
            if(item.isPending()) {
                convertView = LayoutInflater.from(this.getContext())
                        .inflate(R.layout.pending_notification, parent, false);
            } else {
                convertView = LayoutInflater.from(this.getContext())
                        .inflate(R.layout.notification, parent, false);
            }

            viewHolder = new ViewHolder();
            viewHolder.mFirstName = (TextView) convertView.findViewById(R.id.notif_first_name);
            viewHolder.mLastName = (TextView) convertView.findViewById(R.id.notif_last_name);
            viewHolder.mImage = (CircleImageView) convertView.findViewById(R.id.notif_image);
            viewHolder.mEmail = (TextView) convertView.findViewById(R.id.notif_info_text);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        if (item != null) {
            viewHolder.mFirstName.setText(item.getFirst_name());
            viewHolder.mLastName.setText(item.getLast_name());
            viewHolder.mEmail.setText(item.getEmail());


            if(item.getImage_url()!=null)
            Picasso.with(getContext())
                    .load(item.getImage_url())
                    .into(viewHolder.mImage);

            RelativeLayout view = (RelativeLayout) convertView.findViewById(R.id.notif_button_layout);
            view.setVisibility(View.GONE);

            RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParams.addRule(RelativeLayout.BELOW, R.id.notif_info_text);

            if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                layoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
            }
            
            ImageView confirm = (ImageView) convertView.findViewById(R.id.notif_button_confirm);
            confirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (item.isPending()) {
                        nInterface.onConfirmPending(item.getId());
                    } else {
                        if (nInterface != null) {
                            nInterface.onConfirmButtonClicked(position);
                        }
                    }
                }
            });

            RelativeLayout.LayoutParams layoutParamsDismiss = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);

            layoutParamsDismiss.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            layoutParamsDismiss.addRule(RelativeLayout.BELOW,R.id.notif_info_text);
            layoutParamsDismiss.addRule(RelativeLayout.LEFT_OF, confirm.getId());

            if( Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                layoutParamsDismiss.addRule(RelativeLayout.START_OF, confirm.getId());
            }

            ImageView dismiss = (ImageView) convertView.findViewById(R.id.notif_button_dismiss);
            dismiss.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(item.isPending()) {
                        nInterface.onDismissPending(position);
                    } else {
                        if (nInterface != null) {
                            nInterface.onDismissButtonClicked(position);
                        }
                    }
                }
            });

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RelativeLayout b = (RelativeLayout) v;
                    RelativeLayout buttonHolder = (RelativeLayout) b.findViewById(R.id.notif_button_layout);

                    if (buttonHolder.getVisibility()==View.GONE) {
                        buttonHolder.setVisibility(View.VISIBLE);
                    } else {
                        buttonHolder.setVisibility(View.GONE);
                    }
                }


            });
        }

        return convertView;
    }

    public interface NotificationInterface {
        void onConfirmButtonClicked(int position);

        void onDismissButtonClicked(int position);

        void onConfirmPending(int position);

        void onDismissPending(int id);
    }
}
