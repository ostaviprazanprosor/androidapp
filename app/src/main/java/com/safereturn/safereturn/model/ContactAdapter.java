package com.safereturn.safereturn.model;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.safereturn.safereturn.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by maric on 3/24/2016.
 */
public class ContactAdapter extends ArrayAdapter<Contact>  {
    private ViewHolder viewHolder = null;
    private ContactDialogListener mListener = null;

    private static class ViewHolder {
        public CircleImageView mImage;
        public TextView mFirstName;
        public TextView mLastName;
        public TextView mEmail;
    }


    public ContactAdapter(Context context, int resource, ArrayList<Contact> objects,ContactDialogListener listener) {
        super(context,resource,objects);
        this.mListener = listener;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView==null) {

            convertView = LayoutInflater.from(this.getContext())
                    .inflate(R.layout.contact_item, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.mFirstName = (TextView) convertView.findViewById(R.id.contact_item_first_name);
            viewHolder.mLastName = (TextView) convertView.findViewById(R.id.contact_item_last_name);
            viewHolder.mImage = (CircleImageView) convertView.findViewById(R.id.contact_item_image);
            viewHolder.mEmail = (TextView) convertView.findViewById(R.id.contact_item_email);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Contact item = getItem(position);
        if(item!=null) {
            viewHolder.mFirstName.setText(item.getFirst_name());
            viewHolder.mLastName.setText(item.getLast_name());
            viewHolder.mEmail.setText(item.getEmail());

            if(item.getImage_url()!=null)
                Picasso.with(getContext()).load(item.getImage_url()).into(viewHolder.mImage);
            convertView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());

                    builder.setTitle(R.string.pick_option)
                            .setItems(R.array.contact_menu, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    mListener.onDeleteButtonClicked(position);
                                }
                            });
                    builder.show();
                    return false;
                }
            });

        }

        return convertView;
    }

    public interface ContactDialogListener {
        void onDeleteButtonClicked(int id);
    }
}
