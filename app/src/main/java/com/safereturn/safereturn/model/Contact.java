package com.safereturn.safereturn.model;

import java.io.Serializable;

/**
 * Created by maric on 3/23/2016.
 */
public class Contact implements Serializable {
    private String first_name;
    private String last_name;
    private String email;
    private String image_url;
    private int id;
    private boolean pending;

    public Contact(String first_name, String last_name, String email, String image_url, int id) {
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.image_url = image_url;
        this.id = id;
    }

    public Contact(int id) {
        this.id = id;
        this.pending = false;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isPending() {
        return pending;
    }

    public void setPending(boolean pending) {
        this.pending = pending;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", email='" + email + '\'' +
                ", image_url='" + image_url + '\'' +
                ", id=" + id +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Contact contact = (Contact) o;

        return id == contact.id;

    }

    @Override
    public int hashCode() {
        return id;
    }
}
