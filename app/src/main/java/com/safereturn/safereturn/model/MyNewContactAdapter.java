package com.safereturn.safereturn.model;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.safereturn.safereturn.NewContactFragment;
import com.safereturn.safereturn.R;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Contact} and makes a call to the
 * specified {@link com.safereturn.safereturn.NewContactFragment.OnNewContactFragmentInteraction}.
 */
public class MyNewContactAdapter extends RecyclerView.Adapter<MyNewContactAdapter.ViewHolder> {
    private final List<Contact> mValues;
    private final NewContactFragment.OnNewContactFragmentInteraction mListener;

    public MyNewContactAdapter(List<Contact> items, NewContactFragment.OnNewContactFragmentInteraction listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_new_contact, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mFirstName.setText(holder.mItem.getFirst_name());
        holder.mLastName.setText(holder.mItem.getLast_name());
        holder.mEmail.setText(holder.mItem.getEmail());

        Picasso.with(holder.mImage.getContext()).load(holder.mItem.getImage_url()).into(holder.mImage);

        holder.mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    mListener.onNewContactClicked(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mFirstName;
        public final TextView mLastName;
        public final TextView mEmail;
        public final CircleImageView mImage;
        public final Button mButton;
        public Contact mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mFirstName = (TextView) view.findViewById(R.id.new_contact_item_first_name);
            mLastName = (TextView) view.findViewById(R.id.new_contact_item_last_name);
            mEmail = (TextView) view.findViewById(R.id.new_contact_item_email);
            mImage = (CircleImageView) view.findViewById(R.id.new_contact_item_image);
            mButton = (Button) view.findViewById(R.id.add_new_button);
        }

        @Override
        public String toString() {
            return super.toString();
        }
    }
}
